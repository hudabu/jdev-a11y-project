/** This only exists in English, but you can duplicate it and translate it in other languages
 * Then you need to declare the language in app.component.ts */

export const fr = {
  button : {
    more: 'Voir plus',
  },
  headers : {
    home: 'Acceuil',
    list: 'Liste',
    news: 'Actualités',
    about: 'À propos',
    contact: 'Contact',
    login: 'Connexion',
    register: "S'inscrire",
  },
  homepage : {
    titlep1: 'Qui',
    titlep2: 'sommes-nous ?',
    text: "Le site a11y vise à vous montrer les meilleures pratiques en matière de conception Web et d'accessibilité. Nous sommes un site vraiment moderne qui est facilement utilisable par tout le monde. Enfin, presque. Pas vraiment. AU MOINS ON A ESSAYÉ, D'ACCORD !!!!!",
    strength : {
      title: 'Nos points forts',
      card1title : 'Expertise',
      card1text : "Nos développeurs et designers ont tous plus de 20 ans d'expérience. Ils travaillent avec les bases du web.",
      card2title : 'Attention aux détails',
      card2text : 'Nous avons scruté et peaufiné chaque pixel de ce site Web pour offrir la meilleure expérience possible.',
      card3title : 'À jour',
      card3text : "Nous suivons les nouvelles normes telles que HTML 2 et CSS. Nous sommes assez fiers d'être à jour.",
      card4title : 'À jour',
      card4text1 : 'Nous sommes facilement accessibles via le formulaire de contact que vous pouvez atteindre',
      card4text2 : 'ou par e-mail à',
      card4link : 'ici',
    },
    testimonies : {
      title1: 'Sandra, utilisateur a11y',
      text1 : "J'aime ce site Web. Il me montre toutes les couleurs qui existent et qui ne doivent pas être utilisées sur Internet. Naviguer sur ce site Web donne l'impression d'une quête dans l'inconnu. Et tout cela gratuitement.",
      title2: 'Michael, utilisateur a11y',
      text2 : "Depuis que j'ai découvert ce site Web, j'ai perdu 5/10 en moyenne sur mes yeux et j'ai dû acheter des lunettes. C'est un avertissement très agréable sur le type d'émotions et de modifications physiques que de bons sites Web peuvent provoquer.",
    },
    image : {
      title : 'En image',
    },
  },
  aboutPage : {
    title: 'À propos',
    text: "Il n'y a pas grand-chose à voir ici pour l'instant. Pas sûr que vous vouliez savoir qui nous sommes.",
    button: {
      text: 'Revenir'
    }
  }
}
