/** This only exists in English, but you can duplicate it and translate it in other languages
 * Then you need to declare the language in app.component.ts */

export const en = {
  button : {
    more: 'Read more',
  },
  headers : {
    home: 'Home',
    list: 'List',
    news: 'News',
    about: 'About',
    contact: 'Contact',
    login: 'Login',
    register: 'Register',
  },
  homepage : {
    titlep1: 'Who',
    titlep2: 'are we ?',
    text: 'The a11y website aims at showing you the best practices regarding web design and accessibility. We are a really modern website that is easily usable by everyone. Well. Almost. Not really. AT LEAST WE TRIED OK!!!!!',
    strength : {
      title: 'Our strengths',
      card1title : 'Expertise',
      card1text : 'Our developers and designers are all 20+ years of experience. They works with the premises on the web.',
      card2title : 'Attention to details',
      card2text : 'We scrutinized and polished every single pixel of this website for the best experience ever.',
      card3title : 'Up to date',
      card3text : 'We follow the new standards such as HTML 2 and CSS. We\'re quite proud to be that up to date.',
      card4title : 'Up to date',
      card4text1 : 'We are easily accessible through the contact form that you can reach',
      card4text2 : 'or by email at',
      card4link : 'here',
    },
    testimonies : {
      title1: 'Sandra, a11y user',
      text1 : 'I like this website. It shows me all the colors that do exist and are not to be used on the Internet. Navigating this website feels like a quest into the unknown. And all that for free.',
      title2: 'Michael, a11y user',
      text2 : 'Since I discovered this website, I lost 5/10 in average on my eyes and had to buy glasses. This is a very nice warning to what kind of emotions and physical modifications good websites can cause.',
    },
    image : {
      title : 'In image',
      },
  },
  aboutPage : {
    title: 'About',
    firstParagraph: 'Welcome to a11y, where we strive to bring you the best in web design and accessibility practices! We are not just a website; we\'re a commitment to creating a digital space that is inclusive and user-friendly for everyone.',
    secondParagraph: {
      title: 'Our Mission',
      content: 'At a11y, our mission is clear: to make the web accessible to all. We believe that everyone, regardless of their abilities or disabilities, should have equal access to information and services online. We\'re passionate about creating a modern and user-friendly digital experience that goes beyond the ordinary.'
    },
    thirdParagraph: {
      title: 'The a11y Experience',
      content: 'We take pride in being a modern website that embraces innovation and cutting-edge design. Our team is dedicated to crafting an online space that is not only visually appealing but also easily navigable for users of all backgrounds and abilities. We understand the importance of accessibility, and we are committed to providing a platform that is as inclusive as possible.',
    },
    fourthParagraph: {
      title: 'Honesty and Humor',
      content: 'Let\'s be real – achieving perfection in accessibility is no small feat! While we aim for greatness, we\'re not afraid to acknowledge the challenges along the way. Our journey is marked by sincere efforts, occasional hiccups, and a touch of humor. We believe in being transparent about our experiences because, hey, we\'re human too!',
    },
    fifthParagraph: { 
      title: 'At Least We Tried!', 
      content: 'While we may not have achieved absolute perfection (yet!), we want you to know that we\'re giving it our all. "At least we tried" is not just a tagline for us – it\'s a reminder of our commitment to continuous improvement. We\'re on a mission, and we\'re learning and evolving every step of the way.' 
    },
    sixthParagraph: {
      title: 'Join Us on the Journey',
      content: 'Whether you\'re a seasoned web developer, a design enthusiast, or someone just starting their accessibility journey, we invite you to join us. Together, let\'s make the web a more accessible and enjoyable place for everyone. Thanks for being a part of the a11y community!',
    },
  },
  legalNotices: {
    title: 'Legal Notices',
    firstParagraph: 'Welcome to a11y. By accessing and using this website, you agree to comply with and be bound by the following legal notices. If you do not agree with these terms, please do not use this site.',
    secondParagraph: {
      title: 'Terms of Use',
      content: 'The content on this website is for general information purposes only. It is subject to change without notice. We do not provide any warranty or guarantee as to the accuracy, completeness, or suitability of the information found on this site for any particular purpose.'
    },
    thirdParagraph: {
      title: 'Intellectual Property',
      content: 'All content on this website, including text, graphics, logos, and images, is the property of a11y or its content suppliers and is protected by international copyright laws. Unauthorized use or reproduction of any content may violate copyright, trademark, and other laws.'
    },
    fourthParagraph: {
      title: 'Privacy Policy',
      content: 'We respect your privacy. Our Privacy Policy outlines how we collect, use, disclose, and manage your personal information. By using this website, you consent to the terms of our Privacy Policy.'
    },
    fifthParagraph: { 
      title: 'Limitation of Liability', 
      content: 'In no event shall a11y be liable for any direct, indirect, special, consequential, or incidental damages arising out of or in connection with the use or performance of information available on this website.'
    },
    sixthParagraph: {
      title: 'Governing Law',
      content: 'These legal notices are governed by and construed in accordance with the laws of [Your Jurisdiction]. Any disputes relating to these terms and conditions will be subject to the exclusive jurisdiction of the courts in [Your Jurisdiction].',
    },
  }
}
