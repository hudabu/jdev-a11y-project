import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderComponent } from '../../components/header/header.component';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'a11y-about-page',
  standalone: true,
  imports: [CommonModule, TranslateModule, HeaderComponent],
  templateUrl: './about-page.component.html',
  styleUrls: ['./about-page.component.scss'],
})
export class AboutPageComponent {
  constructor(private metaTagService: Meta) {}

  goBack() {
    history.back();
  }

  ngOnInit() {
    this.metaTagService.addTags([
      {
        name: 'description',
        content:
          'This is the About page of our a11y website. Here you can learn more about our mission and the people behind this project.',
      },
    ]);
  }
}
