import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../../components/header/header.component';
import { NewsService } from './news.service';
import { Router } from '@angular/router';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'a11y-news-page',
  standalone: true,
  imports: [CommonModule, HeaderComponent],
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.scss'],
})
export class NewsPageComponent {
  private readonly newsService = inject(NewsService);
  allNews$ = this.newsService.getNews();

  constructor(private router: Router, private metaTagService: Meta) {}

  navigateToNews(id: string) {
    this.router.navigate(['news', id]);
  }

  ngOnInit() {
    this.metaTagService.addTags([
      {
        name: 'description',
        content:
          'This is the News page of our a11y website. Here you can find the latest news about our project.',
      },
    ]);
  }
}
