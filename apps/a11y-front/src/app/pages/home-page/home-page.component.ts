import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterLink} from "@angular/router";
import {HeaderComponent} from "../../components/header/header.component";
import {TranslateModule} from "@ngx-translate/core";
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'a11y-home-page',
  standalone: true,
  imports: [CommonModule, RouterLink, HeaderComponent, TranslateModule],
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent {
  constructor(private metaTagService: Meta) {}

  ngOnInit() {
    this.metaTagService.addTags([
      {
        name: 'description',
        content:
          'The a11y website aims at showing you the best practices regarding web design and accessibility. We are a modern website that is easily usable by everyone.',
      },
    ]);
  }
}
