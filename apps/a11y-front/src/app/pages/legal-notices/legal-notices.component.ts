import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule} from "@ngx-translate/core";
import { HeaderComponent } from '../../components/header/header.component';

@Component({
  selector: 'a11y-legal-notices',
  standalone: true,
  imports: [CommonModule, TranslateModule, HeaderComponent],
  templateUrl: './legal-notices.component.html',
  styleUrls: ['./legal-notices.component.scss'],
})
export class LegalNoticesPageComponent {
  goBack() {
    history.back();
  }
}
