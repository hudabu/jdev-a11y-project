import {Route} from '@angular/router';
import {LegalNoticesPageComponent} from "./legal-notices.component";

export const aboutRoutes: Route[] = [
  {
    path: '',
    component: LegalNoticesPageComponent
  },
];
