import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { HeaderComponent } from '../../components/header/header.component';
import { Meta } from '@angular/platform-browser';

export class ContactFormData {
  constructor(
    public name: string,
    public age: number,
    public email: string,
    public phoneNumber: string,
    public message: string
  ) {}
}

@Component({
  selector: 'a11y-contact-page',
  standalone: true,
  imports: [CommonModule, FormsModule, HeaderComponent],
  templateUrl: './contact-page.component.html',
  styleUrls: ['../../app.component.scss', './contact-page.component.scss'],
})
export class ContactPageComponent {
  constructor(private metaTagService: Meta) {}

  data = new ContactFormData('', 0, '', '', '');
  router = inject(Router);
  submitSuccess = false;

  onSubmit() {
    // alert('Thanks for your message!');
    // console.log('Sent with', JSON.stringify(this.data));
    this.data = new ContactFormData('', 0, '', '', '');
    this.submitSuccess = true;
    // this.router.navigateByUrl('/');
  }

  ngOnInit() {
    this.metaTagService.addTags([
      {
        name: 'description',
        content:
          'This is the Contact page of our a11y website. Here you can send us a message and we will get back to you as soon as possible.',
      },
    ]);
  }
}
