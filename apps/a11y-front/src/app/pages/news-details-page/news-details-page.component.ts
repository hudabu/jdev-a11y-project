import { Component, inject, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { News, NewsService } from '../news-page/news.service';
import { HeaderComponent } from '../../components/header/header.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
import { Meta } from '@angular/platform-browser';

@Component({
  selector: 'a11y-news-details-page',
  standalone: true,
  imports: [CommonModule, HeaderComponent, FooterComponent],
  templateUrl: './news-details-page.component.html',
  styleUrls: ['./news-details-page.component.scss'],
})
export class NewsDetailsPageComponent implements OnInit {
  private readonly newsService = inject(NewsService);
  @Input() id: string = '';

  news$: Observable<News> | undefined;

  currentUrl: string;

  constructor(private location: Location, private metaTagService: Meta) {
    this.currentUrl = window.location.host + this.location.path();
  }

  ngOnInit() {
    this.news$ = this.newsService.getNewsById(this.id);

    this.news$.subscribe((news) => {
      this.metaTagService.addTags([
        {
          name: 'description',
          content: 'Read more about ' + news.title + ' on our a11y website.',
        },
      ]);
    });
  }

  copyLink(url: string) {
    navigator.clipboard
      .writeText(url)
      .then(() => {
        alert('Link copied to clipboard');
      })
      .catch(() => {
        alert('Could not copy text');
      });
  }
}
