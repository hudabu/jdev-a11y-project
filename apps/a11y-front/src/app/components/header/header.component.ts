import {Component, HostListener, inject} from '@angular/core';
import { CommonModule } from '@angular/common';
import {NavigationEnd, Router, RouterLink} from "@angular/router";
import {AuthService} from "../../services/auth.service";
import {TranslateModule, TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'a11y-header',
  standalone: true,
    imports: [CommonModule, RouterLink, TranslateModule],
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  private router = inject(Router);
  authService = inject(AuthService);
  isMenuOpen = false;
  isHome : boolean = false;
  isNews : boolean = false;
  isLogin : boolean = false;
  isRegister : boolean = false;
  isContact : boolean = false;
  isList : boolean = false;
  isScrolled: boolean = false;

  constructor(private translate: TranslateService) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isHome = (event.url === '/');
        this.isContact = (event.url === '/contact');
        this.isList = (event.url === '/lists');
        this.isRegister = (event.url === '/register');
        this.isLogin = (event.url === '/login');
        this.isNews = (event.url === '/news');
      }
    });
  }
  switchLanguage(language: string) {
    this.translate.use(language);
  }
  onChangeLanguage(event: Event) {
    const target = event.target as HTMLSelectElement;
    this.switchLanguage(target.value);
  }
  @HostListener('window:scroll', [])
  onWindowScroll() {
    const offset = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    this.isScrolled = offset > 500;
  }
  toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
    const menu = document.querySelector('.header__menu');
    if (menu) {
      menu.classList.toggle('active', this.isMenuOpen);
    }
  }


  logout() {
    this.authService.logout();
  }

}
